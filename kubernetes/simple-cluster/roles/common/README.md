Role Name
=========

Configurations common to all machines in the Kubernetes cluster

Requirements
------------

N/A

Role Variables
--------------

N/A

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: common }

License
-------

BSD

Author Information
------------------

Brian Hoppus - <brian@hoppus.io>
