# Kubernetes Documentation Notes

## Module 2
```
kubectl version
kubectl get nodes
kubectl run kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1 --port=8080
kubectl get deployments
kubectl proxy
export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{.metadata.name}}{{"\n"}}{{end}}')
echo Name of the Pod: $POD_NAME
curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/proxy/
```

## Module 3

The most common operations can be done with the following kubectl commands:
* kubectl get - list resources
* kubectl describe - show detailed information about a resource
* kubectl logs - print the logs from a container in a pod
* kubectl exec - execute a command on a container in a pod

### New Interactive Tutorial Commands
```
kubectl exec $POD_NAME env
kubectl exec -ti $POD_NAME bash
```

## Module 4
### Creating a New Service
```
kubectl get services
kubectl expose deployment/kubernetes-bootcamp --type="NodePort" --port 8080
kubectl describe services/kubernetes-bootcamp
export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
echo NODE_PORT=$NODE_PORT
curl $(minikube ip):$NODE_PORT
```
### Using Labels
```
kubectl describe deployment
kubctl get pods -l run=kubernetes-bootcamp
kubectl get services -l run=kubernetes-bootcamp
export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
echo Name of the Pod: $POD_NAME
kubectl label pod $POD_NAME app=v1
kubectl describe pod $POD_NAME
kubectl get pods -l app=v1
```
### Deleting a Service
```
kubectl delete service -l run=kubernetes-bootcamp
kubectl get services

```

## Module 5
### Running Multiple Instances of Your App
