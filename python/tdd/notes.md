# Test-Driven Development with Python

## Prerequisites

* Install and run selenium
    
    ``` bash
    brew install selenium-server-standalone geckodriver
    brew cask install java firefox
    selenium-server -port 4444
    ```

* Setup your virtualenv

    ```
    mkdir ~/venv
    virtualenv ~/venv/tdd
    . ~/venv/tdd
    pip install selenium
    pip install Django=1.11
    ```

## Chapter 2
* Functional Tests
  - A methodology that allows developers to see application functionality from a user's point of view
  - Also called acceptance tests, end-to-end tests, and black box tests
  - It tracks a _User Story_
* User Story
  - A description of how the application will work from the point of view of the user
  - Used to structure a functional test
* Expected failure
  - When a test fails in the way that we expected it to

## Chapter 3
* While functional tests are from the user perspective, _unit tests_ are from the programmer perspective
* General workflow from the book
  - write a functional test that fails
  - write unit tests that fail
  - write the smallest amount of application code to pass the unit tests
  - rerun functional test to see if they pass; repeat previous steps as necessary
* Reading Tracebacks
  - Look for the error itself
  - Find which test is failing
  - Look for the place in the test code that resulted in failure
  - look in application code for the problem

# Chapter 4
* The merits of trivial tests
  - If they are really trivial, then it should not take long to write them
  - It is good to have a placeholder
  - The alternative is an arbitrary definition of a function having sufficient complexity that would warrant a test
* Don't test constants
* Refactor: improve the code without changing its functionality

# Chapter 5
* "real" unit tests never touch the database
* Unit tests that interact with databases are called __integration tests__
* Integration tests interact with external systems
* Each unit test should only test one thing
* Regression: When new code breaks some aspect of the applicaiton which used to work
* Red/Green/Refactor: The TDD process. 
  - Write a test to fail (Red)
  - Write some code to get it to pass (Green)
  - Refactor to improve the implementation

# Chapter 6
* Ensure test isolation and managing global state
* Avoid "voodoo" sleeps (magic numbers are generally a bad idea)
* Don't rely on Selenium's implicit waits