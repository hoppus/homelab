#!/bin/bash

source ~/env/terraform-export

terraform apply \
-var "do_token=${DO_TOKEN}" \
-var "pub_key=/Users/brian/.ssh/id_rsa.pub" \
-var "pvt_key=/Users/brian/.ssh/id_rsa" \
-var "ssh_fingerprint=$SSH_FINGERPRINT"
