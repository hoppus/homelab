# NGINX Web Server Deep Dive

## Course Introduction
### Introducing NGINX

#### What is NGINX?
* Open sourced in 2004
* Written to solve the "C10K" problem
* NGINX, Inc. founded to continue development and offer commercial NGINX Plus
* High performance web server
  - 50% of top 1,000 websites run NGINX
* Reverse proxy
  - SSL TLS termination
  - Content caching and compression
* Load balancer

#### What is HTTP?
* Example request

        http://127.0.0.1:8000/users/1

        GET /users/1 HTTP/1.1
        Host:blog.example.com
        User-Agent:curl/7.54.0
        Accept: */*

#### NGINX vs Apache
| NGINX                                     | Apache                                     |
| ----------------------------------------- | ------------------------------------------ |
| nginx config language - directive based   | apache config language - directive based   |
| one processing method                     | multiple processing methods                |
| supports dynamic third party modules      | supports dynamic third party modules       |
| similar performance for dynamic content   | similar performance for dynamic content    |
| faster for serving static files           | slower for serving static files            |
| no concept of .htaccess files             | .htaccess files for local dir config       |

### Installing NGINX
#### CentOS7
Create the repo file at /etc/yum.repos.d/nginx.repo

    [nginx]
    name=nginx repo
    baseurl=http://nginx.org/packages/centos/7/$basearch/
    gpgcheck=0
    enabled=1

Update your local yum db, and install NGINX

    sudo yum update && sudo yum install -y nginx

Start and enable the service

    sudo systemctl start nginx
    sudo systemctl enable nginx

#### Ubuntu 16.04
Add the apt repo key to your system

    curl -o nginx_signing.key http://nginx.org/keys/nginx_signing.key
    sudo apt-key add nginx_signing.key

Append the repo URL to `/etc/apt/sources.list`

    ## Add official NGINX repository
    deb http://nginx.org/packages/ubuntu/ xenial nginx
    deb-src http://nginx.org/packages/ubuntu/ xenial nginx

Update apt and install NGINX

    sudo apt update
    sudo apt install -y nginx

Start and enable NGINX

    sudo systemctl start nginx
    sudo systemctl enable nginx


## NGINX as a Web Server
### Basic Web Server Configuration

#### Understanding the Default NGINX Configuration
* `/etc/nginx` contains the config files
* virtual host config files exist in `/etc/nginx/conf.d`
* main server config file is `/etc/nginx/nginx.conf`
##### nginx.conf Directives outside of Context Blocks
  - user: system user (default is nginx)
  - worker_processes: number of processes to use (1 by default)
  - error_log: log location and level
  - pid: process ID
##### nginx.conf inside Context Blocks
  - events
    * required to run!
    * configures how connections are made to worker processes
    * worker_connections: max number of clients per process (default 1024)
  - http
    * specifies how to handle http connections
    * include: pulls in specified config file
    * default_type: type of file to server
    * log_format: takes a string to define how logs are formatted
    * access_log: where logs go
    * sendfile: enables the `sendfile` system call to deliver content for performance boost in some situations
    * keepalive_timeout: how many seconds a connection can stay open

#### Simple Virtual Host and Server Static Content
```bash
rm conf.d/default.conf
systemctl reload nginx
systemctl status nginx
vim conf.d/default.conf
```

```nginx
server {
    listen 80 default_server;
    server_name _;
    root /usr/share/nginx/html;
}
```
```bash
nginx -t
systemctl reload nginx
curl localhost
vim conf.d/example.com.conf
```
```nginx
server {
    listen 80;
    server_name example.com www.example.com;
    root /var/www/example.com/html;
}
```
```bash
mkdir -p /var/www/example.com/html
echo "<h1>Hello, example.com</h1>" > /var/www/example.com/html/index.html
nginx -t
systemctl reload nginx
systemctl status nginx
curl localhost
curl --header "Host: example.com" localhost
less /var/log/nginx/error.log
semanage fcontext -l | grep /usr/share/nginx/html
semanage fcontext -a -t httpd_sys_content_t '/var/www(/.*)?'
restorecon -R -v /var/www
curl --header "Host: example.com" localhost
```

#### Error Pages
```nginx
server {
    listen 80 default_server;
    server_name _;
    root /usr/share/nginx/html;

    error_page 404 /404.html;
    error_page 500 501 502 503 504 /50x.html;
}
```
```bash
echo "<h1>404 Page Not Found</h1>" > /usr/share/nginx/html/404.html
systemctl reload nginx
curl localhost/fake.html
```

#### Access Control with HTTP Basic Auth
```nginx
server {
    listen 80 default_server;
    server_name _;
    root /usr/share/nginx/html;

    location = /admin.html {
        auth_basic "Login Required";
        auth_basic_user_file /etc/nginx/.htpasswd;
    }

    error_page 404 /404.html;
    error_page 500 501 502 503 504 /50x.html;
}
```
```bash
yum install -y httpd-tools
htpasswd -c /etc/nginx/.htpasswd admin
echo "<h1>Admin Page</h1>" > /usr/share/nginx/html/admin.html
systemctl reload nginx
curl -u admin:password localhost/admin.html
```

### Basic NGINX Security
#### Generating Self-Signed Certificates
```bash
mkdir /etc/nginx/ssl
openssl req -x509 -nodes -days 365 \
-newkey rsa:2048 \
-keyout /etc/nginx/ssl/private.key \
-out /etc/nginx/ssl/public.pem
```

#### Configuring the Host for SSL/TLS/HTTPS
```nginx
server {
    listen 80 default_server;
    listen 443 ssl;
    server_name _;
    root /usr/share/nginx/html;

    ssl_certificate /etc/nginx/ssl/public.pem;
    ssl_certificate_key /etc/nginx/ssl/private.key;

    location = /admin.html {
        auth_basic "Login Required";
        auth_basic_user_file /etc/nginx/.htpasswd;
    }

    error_page 404 /404.html;
    error_page 500 501 502 503 504 /50x.html;
}
```
```bash
nginx -t
systemctl reload nginx
curl -k https://localhost
```

### NGINX Rewrites
#### Cleaning up URLs
```nginx
server {
    listen 80 default_server;
    listen 443 ssl;
    server_name _;
    root /usr/share/nginx/html;

    ssl_certificate /etc/nginx/ssl/public.pem;
    ssl_certificate_key /etc/nginx/ssl/private.key;

    rewrite ^(/.*)\.html(\?.*)?$ $1$2 redirect;
    rewrite ^/(.*)/$ /$1 redirect;

    location / {
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }
    location = /admin {
        auth_basic "Login Required";
        auth_basic_user_file /etc/nginx/.htpasswd;
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }

    error_page 404 /404.html;
    error_page 500 501 502 503 504 /50x.html;
}
```

#### Redirecting All Trafic to HTTPS
```nginx
server {
    listen 80 default_server;
    server_name _;
    return 301 https://$host$request_uri;
}
server {
    listen 443 ssl;
    server_name _;
    root /usr/share/nginx/html;

    ssl_certificate /etc/nginx/ssl/public.pem;
    ssl_certificate_key /etc/nginx/ssl/private.key;

    rewrite ^(/.*)\.html(\?.*)?$ $1$2 redirect;
    rewrite ^/(.*)/$ /$1 redirect;

    location / {
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }
    location = /admin {
        auth_basic "Login Required";
        auth_basic_user_file /etc/nginx/.htpasswd;
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }

    error_page 404 /404.html;
    error_page 500 501 502 503 504 /50x.html;
}
```

### NGINX Modules
#### Overview of NGINX Modules
##### Compiled Modules
```bash
# display all configuration arguments
nginx -V
# list default built-in modules
nginx -V 2>&1 | tr -- - '\n\' | grep _module
```
##### Dynamic Modules
http://nginx.org/en/docs/ngx_core_module.html#load_module

#### Adding Functionality to NGINX with Dynamic Modules
Build the module
```bash
# install pre-requisites
yum groupinstall 'Development tools' -y
yum install -y \
geoip-devel \
libcurl-devel \
libxml2-devel \
libxslt-devel \
libgb-devel \
lmdb-devel \
openssl-devel \
pcre-devel \
perl-ExtUtils-Embed \
yajl-devel \
zlib-devel
# clone Mod-Security, compile, and install
cd /opt
git clone --depth 1 -b v3/master https://github.com/SpiderLabs/ModSecurity
cd ModSecurity
git submodule init
git submodule update
./build.sh
# The 'No names found, cannot describe anything.' error can be dismissed
./configure
make
make install
# download ModSecurity-nginx
cd ../
git clone --depth 1 https://github.com/SpiderLabs/ModSecurity-nginx.git
nginx -v #currently 1.12.2
# download and unpack NGINX source
wget http://nginx.org/donwload/nginx-1.12.2.tar.gz
tar zxvf nginx-1.12.2.tar.gz
# configure and build dynamic module
cd nginx-1.12.2
./configure --with-compat --add-dynamic-moudle=../ModSecurity-nginx
make modules
cp objs/ngx_http_modsecurity_module.so /etc/nginx/modules/
```
Load ModSecurity Module at the top of `/etc/nginx/nginx.conf`
```nginx

user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


# Load ModSecurity dynamic module
load_module /etc/nginx/modules/ngx_http_modsecurity_module.so;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    include /etc/nginx/conf.d/*.conf;
}
```
Enable ModSecurity
```bash
# load the ModSecurity configuration file
mkdir /etc/nginx/modsecurity
cp /opt/ModSecurity/modsecurity.conf-recommended /etc/nginx/modsecurity/modsecurity.conf
```
Send logs to `/var/log/nginx` in `/etc/nginx/modsecurity/modsecurity.conf
```nginx
SecAuditLog /var/log/nginx/modsec_audit.log
```
Enable in primary server block
```nginx
server {
    listen 80 default_server;
    server_name _;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl default_server;
    server_name _;
    root /usr/share/nginx/html;

    modsecurity on;
    modsecurity_rules_file /etc/nginx/modsecurity/modsecurity.conf;

    ssl_certificate /etc/nginx/ssl/public.pem;
    ssl_certificate_key /etc/nginx/ssl/private.key;

    rewrite ^(/.*)\.html(\?.*)?$ $1$2 redirect;
    rewrite ^/(.*)/$ /$1 redirect;

    location / {
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }

    location = /admin {
        auth_basic "Login Required";
        auth_basic_user_file /etc/nginx/.htpasswd;
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }

    error_page 404 /404.html;
    error_page 500 501 502 503 504 /50x.html;
}
```
Check configuration
```bash
nginx -t
systemctl reload nginx
```

## NGINX as a Reverse Proxy
