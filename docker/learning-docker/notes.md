# Learning Docker

## Introduction
### Containers
#### What are Containers?
* operating system virtualization
    - isolated user space instances
    - gives isolated view of processes, user space, and file system
    - shares host Linux kernel
* OS virtualization technologies
    - FreeBSD Jails
    - Solaris Zones
    - LXC

### Docker Architecture
#### What is Docker?
* abstraction on container engines (libvirt, LXC, etc)
* command-line and HTTP API
* standardized packaging for app and libraries
* layered image format
* ecosystem of tools and services
* used for application deployment - "It works on my machine"

## Getting Started
### Docker Machine
* create the virtual machine

        docker-machine create --driver virtualbox dev

* list docker-machines

        docker-machine ls

* connect to your docker machine

        eval $(docker-machine env dev)

* run the test application

        docker run -p 4567:4567 -d rickfast/hello-oreilly-http

* show the ip address of your docker machine

        docker-machine ip dev

* stop your docker machine

        docker-machine stop dev

* remove your docker machine

        docker-machine rm dev


### Getting Started with Containers

        docker run ubuntu pwd
        docker run -i -t ubuntu /bin/bash
        docker run rickfast/hello-oreilly-http
        docker run -d rickfast/hello-oreilly-http
        docker ps
        docker stop a8f3
        docker ps -a
        docker run -d --name hello_container1 rickfast/hello-oreilly-http
        docker restart hello_container1

### Running containerized Web Applications

        docker run -d -P --name redis redis

#### Linking containers

        docker run --link redis -i -t ubuntu /bin/bash
        docker run -p 4567:4567 --link redis --name web rickfast/oreilly-simple-web-app

#### Configuring Containerized Applications

        docker run -e "HELLO=OREILLY" ubuntu /bin/bash -c export

##### How to load data without using links

        docker run -d -e "REDIS_PORT_6379(_TCP_ADDR=172.17.0.2" --name web  -p 4567:4567 rickfast/oreilly-simple-web-app

#### Container Lifecycle
* You can run a previously built container with `docker restart`
* Follow the event logs of a container with `docker logs -f <name-of-container>`

#### Restart Policy
* You can use the restart policy flag to restart a container if it exits

        docker run -d -p 4567:4567 --name timebomb --restart unless-stopped rickfast/oreilly-time-bomb

#### Debugging Containers
* `docker inspect <name-of-container>` returns a JSON payload of useful information
* `docker inspect` also includes filtering

        docker inspect format='{{.NetworkSettings.IPAddress}}' <name-of-contianer>

* `docker exec` can be used to run a secondary process inside an already running container

        docker exec -i -t <name-of-already-running-container> /bin/bash

### Docker Images
#### Creating Docker Image Layers
* You can create Docker images by running containers, changing their state, and saving that state to disk
    - Pick a base image
    - Add a runtime
    - Add libraries and frameworks
    - Start the application
    - Map the appropriate ports
* The `Dockerfile` file format is used to create images built from multiple layers

#### Images on Docker Hub
* Docker Hub is the official public registry of Docker images
* You can pull specific versions of a Docker image by using tags. Latest is the default.
* Download an image with `docker pull <name-of-image>`

##### Building Images
* Choose a base image
* Alpine is a minimal image great for building your own containers

        docker search alpine
        docker pull alpine
        docker run -it alpine /bin/sh
        apk update
        apk add nodejs
        node --version
        mkdir average
        cd average
        vi average.js

* Contents of average.js:

```javascript
#!/usr/bin/env node
var sum = 0;
var count = 0;
process.argv.forEach(function (val, index, array) {
        if(index > 1) {
        sum += parseInt(val);
        count ++;
        }
});
console.log(sum / count);
```

        chmod +x average.js
        ./average.js 3 4 5

* Commit your container state using the container hostname

        exit
        docker commit -m "installed node and wrote average application" <container-image-hostname>
        docker run <new-image-id> average/average.js 3 4 5

#### Dockerfile
* `docker build` is a much more efficent way of building images vs. `docker commit`
* A Dockerfile specifies a list of instructions used to build an image
* This gives us a sharable, reproducable, and automatable way of building images
* Docker takes each instruction and builds them as individual layers on top of each other
* Each line in a Dockerfile is an atomic commit
* Each layer is cached as the image builds; this makes building images fast

        docker build .
        docker build -t hoppus/average .

#### Web Application Images

        docker run -d -p 6379:6379 --name redis redis
        docker build -t hoppus/counter .

#### Build Triggers
* The `ONBUILD` instruction is there for downstream images based on the image built from your Dockerfile

##### Publishing

        docker build -t hoppus/hello .
        docker run -p 5000:5000 hoppus/hello
        docker tag 271f hoppus/hello:1.0
        docker tag 271f hoppus/hello:latest
        docker push hoppus/hello

### Networking
#### Docker Networking
* By default Docker runs 3 networks on the Docker host:

        docker network ls

* The Bridge Network
  - The default network
  - Containers on this network require port mapping to access from outside the host
* The None Network

        docker run -d -P --net none --name no-network-app rickfast/hello-oreilly-http
        docker exec -i -t no-network-app /bin/sh

  - Once inside the container created in the above command, you can see the only network interface is `lo`
* The Host Network
  - Containers run on the same network as the host
  - This means you don't have to expose and map ports

        docker run -d -P --net host --name host-network-app rickfast/hello-oreilly-http

#### User Defined Bridge Networks
* The basic type of user-defined network is the bridge type
* A user-defined bridge network allows you to create a completely isolated network on a single machine

        docker network create --driver bridge my-network
        docker network ls
        docker run -d -P --net my-network --name hello rickfast/hello-oreilly-http
        docker inspect hello

* Overlay networks allow you to define networks that span multiple Docker hosts

#### Docker DNS in User Defined Networks
* User-defined bridge networks cannot use the same linkinng mechanism that the default bridge provides
* Since Docker 1.10, an imbedded DNS server is available for user-defined networks

        docker network create --driver bridge dns-test
        docker network ls
        docker run -d --net dns-test --name dns-test-app rickfast/oreilly-dns-test
        docker run --net dns-test alpine wget -q0- dns-test-app:4567

* You can also use DNS aliases
        docker run -d --net dns-test --name dns-test-app --net-alias dns-alias rickfast/oreilly-dns-test
        docker run --net dns-test alpine wget -q0- dns-alias:4567

### Volumes
#### Data Volumes
* Volumes allow data to persist past the life of a container
* Volumes can also be mounted from directories on the host machine
* Use the `-v` option to mount a directory. The format is `<host-dir>:<container-dir>`

        docker run -d -p 5984:5984 -v $(pwd)/data:/usr/loca/var/lib/couchdb --name couchdb klaemo/couchdb

#### Data Volume Containers
* These are containers that don't run an application; they just house a volume to be shared

        docker create -v /usr/loca/var/lib/couchdb --name db-data debian:jessie /bin/true
        docker run -d -p 5984:5984 -v /usr/loca/var/lib/couchdb --name db1 --volumes-from db-data klaemo/couchdb

### Docker Compose
#### Docker Compose
* Compose files described the relationship between multiple contianers

        docker-compose up
        docker-compose up -d
        docker-compose stop
        docker-compose rm

#### Composing Services
* See docker-compose-example2

#### Building Applications with Docker Compose

        docker-compose up --build

### Docker Extras
#### Docker Swarm
* Seems cool, but the setup is complicated, and Kubernetes is becoming the industry standard
